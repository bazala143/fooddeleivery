import 'dart:async';

import 'package:deliveryboy/src/helpers/maps_util.dart';
import 'package:deliveryboy/src/models/order.dart';
import 'package:deliveryboy/src/repository/search_repository.dart' as sett;
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../repository/settings_repository.dart' as sett;

class MapController extends ControllerMVC {
  Completer<GoogleMapController> controller = Completer();
  GoogleMapController mapController;
  LocationData currentLocation;
  static double _zoom = 15;
  CameraPosition initialCamera = CameraPosition(
    target: LatLng(0.0, 0.0),
    zoom: _zoom,
  );

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  GlobalKey<ScaffoldState> scaffoldKey;

  Set<Polyline> polylines = new Set();

  MapsUtil mapsUtil = new MapsUtil();
  LatLng deliveryLatLong = null;

  MapController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void locationPermission({Order order}) async {
    if (currentLocation == null) {
      Location _locationService = new Location();
      PermissionStatus _permission;
      await _locationService.changeSettings(
          accuracy: LocationAccuracy.HIGH, interval: 1000);
      try {
        bool serviceStatus = await _locationService.serviceEnabled();
        if (serviceStatus) {
          _permission = await _locationService.requestPermission();
          if (_permission == PermissionStatus.GRANTED) {
            currentLocation = await _locationService.getLocation();
            print("Current Location: " +
                currentLocation.latitude.toString() +
                "   " +
                currentLocation.longitude.toString());
            initialCamera = CameraPosition(
              target:
                  LatLng(currentLocation.latitude, currentLocation.longitude),
              zoom: _zoom,
            );
            final GoogleMapController controller = await this.controller.future;
            controller
                .animateCamera(CameraUpdate.newCameraPosition(initialCamera));
            deliveryLatLong = LatLng(
                double.parse(order.deliveryAddress.latitude),
                double.parse(order.deliveryAddress.longitude));
            addMarkers(order);
            getDirectionSteps();
            notifyListeners();
          }
        }
      } catch (e) {
        print("Location error: " + e.toString());
      }
    }
  }

  addMarkers(Order order) async {
    try {
      Map<MarkerId, Marker> newMarkers = <MarkerId, Marker>{};
      MarkerId markerId = MarkerId("101");
      Marker marker = Marker(
        markerId: markerId,
        position: LatLng(currentLocation.latitude, currentLocation.longitude),
        infoWindow: InfoWindow(title: "Current locaion"),
        anchor: Offset(0.5, 0.5),
        icon: BitmapDescriptor.fromAsset("assets/img/ic_delivery.png"),
        onTap: () {},
      );
      newMarkers[markerId] = marker;
      MarkerId markerId1 = MarkerId("102");
      Marker marker1 = Marker(
        markerId: markerId1,
        position: LatLng(deliveryLatLong.latitude, deliveryLatLong.longitude),
        infoWindow: InfoWindow(title: order.deliveryAddress.address),
        anchor: Offset(0.5, 0.5),
        icon: BitmapDescriptor.fromAsset("assets/img/ic_home.png"),
        onTap: () {},
      );
      newMarkers[markerId1] = marker1;
      markers.addAll(newMarkers);
      notifyListeners();
    } catch (e) {
      print("Marker error" + e.toString());
    }
  }

  void getDirectionSteps() async {
    mapsUtil
        .get("origin=" +
            currentLocation.latitude.toString() +
            "," +
            currentLocation.longitude.toString() +
            "&destination=" +
            deliveryLatLong.latitude.toString() +
            "," +
            deliveryLatLong.longitude.toString() +
            "&key=${sett.setting.value?.googleMapsKey}")
        .then((dynamic res) {
      List<LatLng> _latLng = res as List<LatLng>;
      _latLng.insert(
          0, new LatLng(currentLocation.latitude, currentLocation.longitude));
      setState(() {
        polylines.add(new Polyline(
            visible: true,
            polylineId: new PolylineId(currentLocation.hashCode.toString()),
            points: _latLng,
            color: Color(0xFFea5c44),
            width: 6));
      });
    });
  }
}
