import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class CustomerProfileController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;

  CustomerProfileController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }
}
