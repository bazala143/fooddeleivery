import 'dart:convert';

import 'package:deliveryboy/src/models/google_data.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../generated/i18n.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as repository;

class UserController extends ControllerMVC {
  User user = new User();
  bool hidePassword = true;
  GlobalKey<FormState> loginFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  FirebaseMessaging _firebaseMessaging;
  TextEditingController userNameController = new TextEditingController();
  TextEditingController userEmailController = new TextEditingController();
  bool isUserApiLoading = false;

  /* this code is for native ios google login platform channel*/
  BuildContext mContext;
  bool isFromLogin = false;
  static const platformChannel =
      const MethodChannel('com.foodzela.deliveryboy.channel');

  UserController() {
    loginFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.getToken().then((String _deviceToken) {
      user.deviceToken = _deviceToken;
    });
  }

  void login({bool isFromregister}) async {
    isUserApiLoading = true;
    notifyListeners();
    if (isFromregister != null && isFromregister) {
      repository.login(user).then((value) {
        if (value != null && value.apiToken != null) {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(S.current.welcome + value.name),
          ));
        } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(S.current.wrong_email_or_password),
          ));
        }
        isUserApiLoading = false;
        notifyListeners();
        Navigator.of(scaffoldKey.currentContext)
            .pushReplacementNamed('/Pages', arguments: 2);
      });
    } else if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      repository.login(user).then((value) {
        //print(value.apiToken);

        if (value != null && value.apiToken != null) {
          scaffoldKey?.currentState?.showSnackBar(SnackBar(
            content: Text(S.current.welcome + value.name),
          ));
          Navigator.of(scaffoldKey.currentContext)
              .pushReplacementNamed('/Pages', arguments: 1);
        } else {
          scaffoldKey?.currentState?.showSnackBar(SnackBar(
            content: Text(S.current.wrong_email_or_password),
          ));
        }
        isUserApiLoading = false;
        notifyListeners();
      });
    }
  }

  void register({bool isSocial}) async {
    isUserApiLoading = true;
    notifyListeners();
    if (isSocial != null && isSocial) {
      repository.register(user).then((value) {
        isUserApiLoading = false;
        notifyListeners();
        if (value != null && value.apiToken != null) {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(S.current.welcome + value.name),
          ));
          Navigator.of(scaffoldKey.currentContext)
              .pushReplacementNamed('/Pages', arguments: 2);
        } else {
          login(isFromregister: true);
          /* scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Email Already registered."),
          ));*/
        }
      });
    } else if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      repository.register(user).then((value) {
        if (value != null && value.apiToken != null) {
          scaffoldKey?.currentState?.showSnackBar(SnackBar(
            content: Text(S.current.welcome + value.name),
          ));
          Navigator.of(scaffoldKey.currentContext)
              .pushReplacementNamed('/Pages', arguments: 1);
        } else {
          scaffoldKey?.currentState?.showSnackBar(SnackBar(
            content: Text(S.current.wrong_email_or_password),
          ));
        }
        isUserApiLoading = false;
        notifyListeners();
      });
    }
  }

  void resetPassword() {
    if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      repository.resetPassword(user).then((value) {
        if (value != null && value == true) {
          scaffoldKey?.currentState?.showSnackBar(SnackBar(
            content:
                Text(S.current.your_reset_link_has_been_sent_to_your_email),
            action: SnackBarAction(
              label: S.current.login,
              onPressed: () {
                Navigator.of(scaffoldKey.currentContext)
                    .pushReplacementNamed('/Login');
              },
            ),
            duration: Duration(seconds: 10),
          ));
        } else {
          scaffoldKey?.currentState?.showSnackBar(SnackBar(
            content: Text(S.current.error_verify_email_settings),
          ));
        }
      });
    }
  }

  void loginWithGoogle() {
    repository.startGoogleLogin().then((googleSignInAccount) {
      if (googleSignInAccount != null) {
        user.email = googleSignInAccount.email;
        user.loginType = "Google";
        isUserApiLoading = true;
        notifyListeners();
        repository.login(user).then((value) {
          if (value != null && value.apiToken != null) {
            scaffoldKey?.currentState?.showSnackBar(SnackBar(
              content: Text(S.current.welcome + value.name),
            ));
            Navigator.of(scaffoldKey.currentContext)
                .pushReplacementNamed('/Pages', arguments: 1);
          } else {
            scaffoldKey.currentState.showSnackBar(SnackBar(
              content: Text("Email is not Registered. Register now"),
              action: SnackBarAction(
                label: S.current.register,
                onPressed: () {
                  user.email = googleSignInAccount.email;
                  user.name = googleSignInAccount.displayName;
                  user.loginType = "Google";
                  user.password = user.password =
                      googleSignInAccount.email.split("@")[0] + "@123";
                  register(isSocial: true);
                },
              ),
              duration: Duration(seconds: 10),
            ));
          }
          isUserApiLoading = false;
          notifyListeners();
        });
      } else {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(S.current.wrong_email_or_password),
        ));
      }
    });
  }

  void registerWithGoogle() {
    repository.startGoogleLogin().then((googleSignInAccount) {
      if (googleSignInAccount != null) {
        user.email = googleSignInAccount.email;
        user.name = googleSignInAccount.displayName;
        user.password =
            user.password = googleSignInAccount.email.split("@")[0] + "@123";
        user.loginType = "Google";
        print("Register Api Params = > " + jsonEncode(user.toMap()));
        register(isSocial: true);
        notifyListeners();
      } else {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text("Unable to get email address"),
        ));
      }
    });
  }

  /*integrate to native ios google login using platform channel*/

  void attchContext(BuildContext context) {
    this.mContext = context;
  }

  void iosGoogleLogin(bool isFromLogin) {
    this.isFromLogin = isFromLogin;
    platformChannel.invokeMethod("GoogleLogin");
  }

  void iosAppleLogin(bool isFromLogin) {
    this.isFromLogin = isFromLogin;
    platformChannel.invokeMethod("AppleLogin");
  }

  void initPlatFormChannel() {
    platformChannel.setMethodCallHandler(_handleMethod);
  }

  Future<dynamic> _handleMethod(MethodCall call) async {
    switch (call.method) {
      case "GoogleLogin":
        _redirectToLoginSignUp(call.arguments, false);
        return new Future.value("");
      case "AppleLogin":
        _redirectToLoginSignUp(call.arguments, true);
        return new Future.value("");
    }
  }

  Future<void> _redirectToLoginSignUp(String data, bool isGoogleLogin) async {
    print("Google Data " + data);
    Map<String, dynamic> dataMap = json.decode(data);
    GoogleData googleData = GoogleData.fromJson(dataMap);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("IsGoogleLogin " + isGoogleLogin.toString());
    if (!isGoogleLogin) {
      if (googleData.email != null) {
        prefs.setString("appleData", jsonEncode(googleData));
      } else {
        String appleData = prefs.getString("appleData");
        googleData = GoogleData.fromJson(jsonDecode(appleData));
      }
      print("AppleData ===> " + jsonEncode(googleData));
    }
    if (googleData != null) {
      if (isFromLogin) {
        loginWithGoogleIOS(googleData);
      } else {
        user.email = googleData.email;
        user.name = googleData.name;
        user.password = googleData.email.split("@")[0] + "@123";
        user.loginType = "Google";
        print("Register Api Params ===> " + jsonEncode(user.toMap()));
        register(isSocial: true);
      }
    }
  }

  void loginWithGoogleIOS(GoogleData googleData) {
    user.email = googleData.email;
    user.loginType = "Google";
    isUserApiLoading = true;
    notifyListeners();
    repository.login(user).then((value) {
      if (value != null && value.apiToken != null) {
        scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(S.current.welcome + value.name),
        ));
        Navigator.of(scaffoldKey.currentContext)
            .pushReplacementNamed('/Pages', arguments: 2);
      } else {
        scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Email is not registered. Register now"),
          action: SnackBarAction(
            label: S.current.register,
            onPressed: () {
              user.email = googleData.email;
              user.name = googleData.name;
              user.password =
                  user.password = googleData.email.split("@")[0] + "@123";
              user.loginType = "Google";
              print("Register Api Params = > " + jsonEncode(user.toMap()));
              register(isSocial: true);
            },
          ),
          duration: Duration(seconds: 10),
        ));
      }
      isUserApiLoading = false;
      notifyListeners();
    });
  }
}
