import 'package:deliveryboy/generated/i18n.dart';
import 'package:deliveryboy/src/controllers/map_controller.dart';
import 'package:deliveryboy/src/elements/DrawerWidget.dart';
import 'package:deliveryboy/src/models/order.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class OrderMap extends StatefulWidget {
  Order order;
  OrderMap(this.order);

  @override
  _OrderMapState createState() => _OrderMapState();
}

class _OrderMapState extends StateMVC<OrderMap> {
  MapController _con;

  _OrderMapState() : super(MapController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      _con.locationPermission(order: widget.order);
    });
  }

  @override
  Widget build(BuildContext context) {
    final googleMap = GoogleMap(
      mapType: MapType.normal,
      compassEnabled: false,
      myLocationEnabled: true,
      initialCameraPosition: _con.initialCamera,
      polylines: _con.polylines,
      onCameraIdle: () {
        debugPrint("camera idle");
      },
      onCameraMove: (position) {},
      myLocationButtonEnabled: false,
      markers: Set<Marker>.of(_con.markers.values),
      onMapCreated: (GoogleMapController googleMapController) {
        _con.mapController = googleMapController;
        if (!_con.controller.isCompleted)
          _con.controller.complete(googleMapController);
      },
    );

    return Scaffold(
        key: _con.scaffoldKey,
        drawer: DrawerWidget(),
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
            onPressed: () => _con.scaffoldKey?.currentState?.openDrawer(),
          ),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text(
            S.of(context).order_details,
            style: Theme.of(context)
                .textTheme
                .title
                .merge(TextStyle(letterSpacing: 1.3)),
          ),
        ),
        body: googleMap);
  }
}
