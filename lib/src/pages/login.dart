import 'package:deliveryboy/src/elements/GoogleButton.dart';
import 'package:deliveryboy/src/elements/apple_signin_button.dart';
import 'package:deliveryboy/src/helpers/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/i18n.dart';
import '../controllers/user_controller.dart';
import '../elements/BlockButtonWidget.dart';
import '../helpers/app_config.dart' as config;
import '../repository/user_repository.dart' as userRepo;

class LoginWidget extends StatefulWidget {
  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends StateMVC<LoginWidget> {
  UserController _con;
  BuildContext mContext;

  _LoginWidgetState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.initPlatFormChannel();
    if (userRepo.currentUser.value?.apiToken != null) {
      Navigator.of(context).pushReplacementNamed('/Pages', arguments: 1);
    }
  }

  @override
  Widget build(BuildContext context) {
    final progressBar = Center(
      child: CircularProgressIndicator(),
    );
    final btnLogin = BlockButtonWidget(
      text: Text(
        S.of(context).login,
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      color: Theme.of(context).accentColor,
      onPressed: () {
        _con.login(isFromregister: false);
      },
    );

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _con.scaffoldKey,
        resizeToAvoidBottomPadding: false,
        body: Builder(builder: (context) {
          mContext = context;
          _con.attchContext(context);
          return SingleChildScrollView(
            child: Container(
              height: 770,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                alignment: AlignmentDirectional.topCenter,
                children: <Widget>[
                  Positioned(
                    top: 0,
                    child: Container(
                      width: config.App(context).appWidth(100),
                      height: config.App(context).appHeight(37),
                      decoration:
                          BoxDecoration(color: Theme.of(context).accentColor),
                    ),
                  ),
                  Positioned(
                    top: config.App(context).appHeight(37) - 140,
                    child: Container(
                      width: config.App(context).appWidth(84),
                      height: config.App(context).appHeight(37),
                      child: Text(
                        S.of(context).lets_start_with_login,
                        style: Theme.of(context).textTheme.display3.merge(
                            TextStyle(color: Theme.of(context).primaryColor)),
                      ),
                    ),
                  ),
                  Positioned(
                    top: config.App(context).appHeight(37) - 50,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 50,
                              color:
                                  Theme.of(context).hintColor.withOpacity(0.2),
                            )
                          ]),
                      margin: EdgeInsets.symmetric(
                        horizontal: 20,
                      ),
                      padding:
                          EdgeInsets.symmetric(vertical: 50, horizontal: 27),
                      width: config.App(context).appWidth(88),
//              height: config.App(context).appHeight(55),
                      child: Form(
                        key: _con.loginFormKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            TextFormField(
                              enabled: _con.isUserApiLoading ? false : true,
                              keyboardType: TextInputType.emailAddress,
                              onSaved: (input) => _con.user.email = input,
                              validator: (input) {
                                if (CommonUtils.isValidEmail(input.trim())) {
                                  return null;
                                } else {
                                  return S.of(context).should_be_a_valid_email;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: S.of(context).email,
                                labelStyle: TextStyle(
                                    color: Theme.of(context).accentColor),
                                contentPadding: EdgeInsets.all(12),
                                hintText: 'johndoe@gmail.com',
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.7)),
                                prefixIcon: Icon(Icons.alternate_email,
                                    color: Theme.of(context).accentColor),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.2))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.5))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.2))),
                              ),
                            ),
                            SizedBox(height: 30),
                            TextFormField(
                              enabled: _con.isUserApiLoading ? false : true,
                              keyboardType: TextInputType.text,
                              onSaved: (input) => _con.user.password = input,
                              validator: (input) => input.length < 3
                                  ? S
                                      .of(context)
                                      .should_be_more_than_3_characters
                                  : null,
                              obscureText: _con.hidePassword,
                              decoration: InputDecoration(
                                labelText: S.of(context).password,
                                labelStyle: TextStyle(
                                    color: Theme.of(context).accentColor),
                                contentPadding: EdgeInsets.all(12),
                                hintText: '••••••••••••',
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.7)),
                                prefixIcon: Icon(Icons.lock_outline,
                                    color: Theme.of(context).accentColor),
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _con.hidePassword = !_con.hidePassword;
                                    });
                                  },
                                  color: Theme.of(context).focusColor,
                                  icon: Icon(_con.hidePassword
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                ),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.2))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.5))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.2))),
                              ),
                            ),
                            SizedBox(height: 30),
                            _con.isUserApiLoading ? progressBar : btnLogin,
                            SizedBox(height: 10),
                            GoogleButton(
                              color: Colors.grey,
                              onPressed: () {
                                if (CommonUtils.isAndroidPlatform()) {
                                  _con.loginWithGoogle();
                                } else {
                                  _con.iosGoogleLogin(true);
                                }
                              },
                            ),
                            SizedBox(height: 15),
                            Visibility(
                              child: AppleSignInButton(
                                style: ButtonStyle.black,
                                type: ButtonType.signIn,
                                cornerRadius: 5,
                                onPressed: () {
                                  _con.iosAppleLogin(true);
                                },
                              ),
                              visible: true,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 70,
                    child: Column(
                      children: <Widget>[
                        FlatButton(
                          onPressed: () {
                            // Navigator.of(context).pushReplacementNamed('/SignUp');
                          },
                          textColor: Theme.of(context).hintColor,
                          child: Text(S.of(context).i_forgot_password),
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.of(context)
                                .pushReplacementNamed('/SignUp');
                          },
                          textColor: Theme.of(context).hintColor,
                          child: Text(S.of(context).i_dont_have_an_account),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
