
import 'dart:io';

class CommonUtils{
  static bool isAndroidPlatform(){
    if(Platform.isAndroid){
      return true;
    }else{
      return false;
    }
  }
  static bool isValidEmail(String email){
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
    return emailValid;
  }

}